+++
title = "Drupal WebForm Module hiccups"
date = "2012-06-09"

categories = []
description = ""
tags = []
+++

For the past few days, I have been reading countless about making web forms with drupal.  My task was to make a web form that stored submissions in a database that can easily be accessed by external services.  Luckily, there's an awesome WebForm module that did most of the heavy lifting for me.  In fact, it even saved all the data into its own database.

Unfortunately, the submission data was laid out in a convoluted way, and it was split between two tables, and moreover.  When you are specifying options for a select field in the web form, the webform_submitted_data table only contains the keys whereas the webform_components table contains the mapping in a general 'extra' field string.  At first it might seem like there will be data integrity issues if the select option key-value pairs change over time.  If 1 maps to 'france' then there's a row in the data table with a value of 1 for that option.  If I change 1 to map to 'germany', that record in the data table is now invalid.  You can read more about the issue and resolution here, but essentially thats the intended behavior.  

I needed to save the values in a data table, not the keys.  So there are a couple ways to solve this:

Parse the mapping string with SQL and join it with the oddly structured data table... *shudder*
Hack away at the webform hooks to save the data in my own table.  
Disregarding the messy first method, I went ahead and made a module to hook into WebForm.  Everything was fine until I ran into problem parsing the key option mappings...

And so after reading the issue again, someone suggested making the keys the same as the values.  Then the table has the same data as value.  Such a simple solution.  If only I read the issue thread more closely, I would have saved a lot of headaches and time.

I still had to deal with the nonstandard layout of the submission data table, but thankfully I found the webform_mysql_views module, which essentially solved the problem for me.  Again I was saved by reading resources more closely.

I learned a few things from this mini-project. First, RTFM; always read things closely.  Second, if you want a task done in Drupal, chances are there's a module for that, so keep looking.   And lastly, the Drupal community is great but as a framework, I feel that the learning curve is very high for beginners compared to other frameworks like rails.  However I can see why Drupal is still a great choice for many sites who need a robust, flexible CMS.  
