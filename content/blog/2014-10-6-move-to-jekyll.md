---
title:            "A New Beginning"
date:            2014-10-06
description:     "Moving to a new Jekyll site"
tags:             ["jekyll", "update"]
categories:       blog
---

Back in 2012, I started to learn about web development and decided to make my
own personal website.  My only criteria for the website was that it could handle
blog posts and other simple pages such as project links.  I wanted to use a more
'hackerish' framework, so I went with [Octopress](http://octopress.org/) at the time.
Octopress uses [Jekyll](http://jekyllrb.com/), but takes care of configuration,
HTML/CSS templates, js, etc. for you.  And it comes with a nice default theme
[^5].

However after spending some time with it, I found that there was too much bloat.
I recently decided to switch over to Jekyll and start building out the look of my 
site from scratch.

# Design
To come up with a new design, I went on a browsing binge and googled different
styles.  Here are just a few sites I drew inspiration from:

- [hikari theme](http://m3xm.github.io/hikari-for-Jekyll/)
- [hpster theme](http://mmistakes.github.io/hpstr-jekyll-theme/about/)
- [camporez](http://camporez.com/)
- [dbyll](http://dbtek.github.io/dbyll/)
- [medium.com](https://medium.com/)
- [mademistakes.com](http://mademistakes.com/)


# Jekyll

## Portfolio
One of the main reasons I have a personal website is to show off my past and
future projects.  When coming up with the site structure, I thought about adding
a portfolio section with project posts [^1].  However I decided it was a lot
simpler to stick with a simple list of links to my projects instead.  I figured
if I wanted to, I could always manually add a link to a project post in `_data/projects.yml`.


## Liquid Tags
http://docs.shopify.com/themes/liquid-documentation (liquid tags)
Maybe I've been spoiled by good docs from places like Apple's iOS documentation 
and the Ruby on Rails, but the documentation for liquid tags leaves a lot to be desired.  I
found that it didn't have enough examples and was not clear enough for some simple
tasks.

Here is a small list of gotchas I personally encountered:

1. Watch out for 'if' tag in 'capture' tag. Found out that the 'capture' tag will capture empty strings if the 'if' statement is not true.

2. And also, why doesn't liquid tags have a '!' (not)  operator?

3. Condition statment in this if tag does not support parens?

```
{% if (... and ...) or (... and ...) %}
  do something
{% endif %}
```



# CSS

## Gotchas

I wanted to close the side nav menu when the user resizes the site from small
to big.  So I figured I could write some small javascript code to accomplish
this.  However this approach isn't the best because:

1. It fires a lot of unnecessary events if the user often resizes browser.

2. Since I'm 'closing' the menu when I resize the window to a larger width, I
also need to add more logic to 'open' the menu again when I resize the window
back to a smaller width.  And that begs the question, shouldn't the state of a nav menu remain the same whether or not the window is large or small? If the user has a menu open, he/she should expect the menu to stay open when the window resizes. 

It makes logical sense, albeit changes my site design.  Now the site will have
potentially two sections to access the page links.  I think a great way to solve
this problem is to forego changing the state of the nav menu, and just create a
side menu for the list of page links [^2].

So essentially I have two menus to access page links: One on the left side that can be opened and closed, and a regular one on top that only appears when the browser window is big enough.  This avoids the messy logic of keeping a nav menu open or not when resizing the window.


Although this solution leads to a bit of code duplication, it gives the user
more ways to navigate.  If we wanted to, we can extract out the logic of listing
out pages.

# Javascript

## Transitions
For the top header, I found a great blog post detailing how to make a header hide
when the user scrolls down [^3].  I made some small modifications to make the
hiding behavior suit my needs.

While styling the website,  I found a visual bug where the sidebar would show
pop in and out when resizing the page.  Turns out it was because the 'left' css
attribute for the side bar was changing due to media queries, and the default
css for the sidebar had a transition attribute defined that applies anytime the
'left' attribute changed.  

I needed to somehow turn off css animations for the element when the sidebar is not open.  Nothing that a little JS can't fix.  I eventually figured out I needed to add the transition attribute right after the user clicked the open button, and then remove the attirbute once the transition ended.  And what do you know, browsers have transitionEnd events for us to
listen to [^4]. 

## Media Queries
I wanted to dynamically adjust the comments height for certain window widths,
so I learned about the Media Query API [^6].  I was familiar with media queries in 
CSS, but never worked with them in javascript code.  After reading a 
(brief introduction to media queries in javascript)[mq-js], I got the concept
fairly quickly.  The biggest benefit I see in using them is that it provides
a consistent, cross-browser solution to handling different window widths.

## Misc. 
I got a big refresher on everything about js while working on this website.
Concepts such as caveats of `this`, obscure details about Event Handling, 
DOM manipulation, and all those frustating, seemingly random, idiosyncratic javascript behaviors.
It's good to be back in web development.

# Summary
Overall, I am glad I decided to redesign my personal website.   I still have 
to work on other aspects of the design, such as typography, but for now it's 
good enough to put up live.  There were a lot of frustrating moments during
development, but I learned a lot during the process.  I put in a lot more hours
than I thought I would have, but it was worth the pain and frustration to 
refresh myself on front end development.  Let's hope I keep at this in the future.


# Reference

Before I decided to start over with developing my site, I wanted to learn the best
practices to making web sites.  Below are some of the many resources and pages I
looked into.

## CSS best practices:

- [SMACSS Guide][smacss]
- [How to scale and maintain legacy css with sass and smaccs][envato] 
- [SASS and CSS Practices - Nesting variables, mixins, etc.][jb] 
- [Don't nest CSS][dnc]
- [How to structure a Sass Project][sassway]

## Misc tools
- [Setting up Livereload with Jekyll][live]
- [Font Awesome][fa]


## Footnotes

---

[^5]: It was so nice in fact that the majority of Octopress blogs on the web all looked the same.
[^1]: http://www.divshot.com/blog/web-development/advanced-jekyll-features/#custom-post-types
[^2]: http://tympanus.net/codrops/2013/04/17/slide-and-push-menus/
[^3]: https://medium.com/@mariusc23/hide-header-on-scroll-down-show-on-scroll-up-67bbaae9a78c
[^4]: http://blog.teamtreehouse.com/using-jquery-to-detect-when-css3-animations-and-transitions-end
[^6]: https://developer.mozilla.org/en-US/docs/Web/Guide/CSS/Media_queries



[envato]:   http://webuild.envato.com/blog/how-to-scale-and-maintain-legacy-css-with-sass-and-smacss/
[smacss]:   https://smacss.com/
[jb]:       http://joshbroton.com/my-sass-less-css-practices-modularization-nesting-variables-mixins-etc/ 
[dnc]:      http://sriharisriraman.in/blog/2013/09/08/dont-nest-css/
[sassway]:  http://thesassway.com/beginner/how-to-structure-a-sass-project


[live]:     http://dan.doezema.com/2014/01/setting-up-livereload-with-jekyll/
[fa]:       https://fortawesome.github.io/Font-Awesome/
