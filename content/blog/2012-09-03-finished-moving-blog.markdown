---
title: "Finished Moving Blog"
date: 2012-09-03
categories: blog
---

This weekend, I finally got a chance to move my blog over from posterous to octopress. I had the bad luck of making a posterous blog right before twitter acquired them earlier this year. And so fearing that posterous will just rot away, I've been looking for blogging alternatives.<!--more--> Although I could have easily made a free blog on wordpress.com or blogger, I decided I wanted to be in charge of my own data and not have my blog in the mercy of other companies. After some research, I found [Octopress](http://octopress.org/) and am excited to see how using this blogging framework will turn out.
